import sys
import time
import subprocess
from os.path import realpath, dirname, join as joins

file_path = realpath(__file__)
app_dir = dirname(dirname(file_path))
app_path = joins(app_dir, "src", "app.py")
test_path = joins(dirname(file_path), "test_app.py")
process = subprocess.Popen(["python3", app_path])
time.sleep(3)
result = subprocess.run(["pytest", test_path])
sys.exit(result.returncode)